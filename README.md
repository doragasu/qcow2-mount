# qcow2-mount

Quick script to mount qcow2 files as Network Block Device filesystems

## Usage

```
qcow-mount -q qcow2_file [OPTIONS]

Supported options:
-h: Print this help screen
-q qcow2_file: File to mount. Mandatory options.
-m mount_point: Set mount point (default: /mnt)
-d nbd_device_num: nbd debice (default: 0 for nbd0)
-p partition: Partition number to mount (default: 1)
-x max_parts: Maximum number of partitions (default: 8)
-o mount_opts: Mount options (default uid=1000,gid=1000)
-t timemout: Seconds to wait for the nbd device (default: 10)
```

Example: mount file.qcow2 into /mnt/test

```
# qcow-mount -q file.qcow2 -m /mnt/test
```

